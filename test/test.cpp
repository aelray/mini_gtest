#include <gtest/gtest.h>
#include "lib.h"


TEST(MyLibTest, print_hello_world){
  print_hello_world();
}

TEST(MyLibTest, test_add){
  EXPECT_EQ(add_numbers(1, 2), 3);
}